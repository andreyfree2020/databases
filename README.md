## PostgreSQL
- [Документация к PostgreSQL на русском](https://postgrespro.ru/docs/postgrespro/14/index)  
- [Установка от ruvds](https://ruvds.com/ru/helpcenter/postgresql-pgadmin-ubuntu/)  
- [Что такое pg_hba](https://postgrespro.ru/docs/postgrespro/10/auth-pg-hba-conf)  
- [Как работать с пользователями в Postgres](https://www.dmosk.ru/miniinstruktions.php?mini=postgresql-users)  
- [Про отказоустойчивость в БД](https://postgrespro.ru/docs/postgrespro/14/high-availability)  
- [Настройка репликации в Postgres](https://selectel.ru/blog/tutorials/how-to-set-up-replication-in-postgresql/)  
- [Потоковая репликация](https://www.postgresql.fastware.com/postgresql-insider-ha-str-rep#:~:text=Streaming%20replication%2C%20a%20standard%20feature,can%20be%20kept%20in%20sync.), [Пример для 9.5](https://eax.me/postgresql-replication/), и [пример на Русском](https://selectel.ru/blog/tutorials/how-to-set-up-replication-in-postgresql/)
